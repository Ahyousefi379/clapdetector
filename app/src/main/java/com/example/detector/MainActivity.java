package com.example.detector;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.SpectralPeakProcessor;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.onsets.OnsetHandler;
import be.tarsos.dsp.onsets.PercussionOnsetDetector;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_AUDIO_PERMISSION_RESULT = 1;

    Drawable color;
    double threshold;
    double sensitivity;
    TextView sensitivityTv, thresholdTv;
    SeekBar thresholdSb, sensitivitySb;
    Button startBtn;
    EditText delay_et, phoneNumber_et;
    int x;
    AudioDispatcher dispatcher;
    boolean isRecording = false, haveThread = false, hasDetected = false;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    Thread thread;
    int claps = 0;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hasDetected = false;
        isRecording = false;
        if (haveThread) {
            thread.interrupt();
            haveThread = false;
        }
        claps = 0;

        sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
        delay_et = findViewById(R.id.delay_et);
        phoneNumber_et = findViewById(R.id.PhoneNumber_et);
        sensitivityTv = findViewById(R.id.sensitivityTextview);
        sensitivitySb = findViewById(R.id.sensitivitySeekbar);
        thresholdTv = findViewById(R.id.ThresholdTv);
        thresholdSb = findViewById(R.id.ThresholdSeekbar);
        sensitivity = sharedPreferences.getInt("SENSITIVITY", 50);
        sensitivitySb.setProgress((int) sensitivity);
        if (sharedPreferences.contains("phoneNumber")) {
            phoneNumber_et.setText(sharedPreferences.getString("phoneNumber", ""));
        }
        if (sharedPreferences.contains("delay")) {
            delay_et.setText(sharedPreferences.getString("delay", ""));
        }
        sensitivityTv.setText("Sensitivity = " + sensitivity + "%");
        threshold = sharedPreferences.getInt("THRESHOLD", 12);
        thresholdTv.setText("Threshold = " + threshold);
        thresholdSb.setProgress((int) threshold);
//        stopBtn = findViewById(R.id.stopAlarmBtn);
        startBtn = findViewById(R.id.startButton);
        color = startBtn.getBackground();
        if (color instanceof ShapeDrawable) {
            ((ShapeDrawable) color).getPaint().setColor(ContextCompat.getColor(this, R.color.green));
        } else if (color instanceof GradientDrawable) {
            ((GradientDrawable) color).setColor(ContextCompat.getColor(this, R.color.green));
        } else if (color instanceof ColorDrawable) {
            ((ColorDrawable) color).setColor(ContextCompat.getColor(this, R.color.green));
        }
        startBtn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if (!isRecording) {
                    hasDetected = false;
                    startRecording();
                    claps = 0;
                    isRecording = true;
                    if (!delay_et.getText().toString().isEmpty()) {
                        editor.putString("delay", delay_et.getText().toString().trim());
                        editor.apply();
                    }

                    editor.putString("phoneNumber", phoneNumber_et.getText().toString().trim());
                    editor.apply();

                    if (color instanceof ShapeDrawable) {
                        ((ShapeDrawable) color).getPaint().setColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                    } else if (color instanceof GradientDrawable) {
                        ((GradientDrawable) color).setColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                    } else if (color instanceof ColorDrawable) {
                        ((ColorDrawable) color).setColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                    }
                    startBtn.setText("Stop");

                } else {
                    if (isRecording) {
                        hasDetected = false;
                        claps = 0;
                        if (color instanceof ShapeDrawable) {
                            ((ShapeDrawable) color).getPaint().setColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                        } else if (color instanceof GradientDrawable) {
                            ((GradientDrawable) color).setColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                        } else if (color instanceof ColorDrawable) {
                            ((ColorDrawable) color).setColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                        }
                        isRecording = false;
                        thread.interrupt();
                        haveThread = false;
                        startBtn.setText("Start");
                    }

                }

            }
        });
        sensitivitySb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                x = i;
                sensitivityTv.setText("sensitivity = " + i + "%");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (haveThread) {
                    thread.interrupt();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                sensitivity = x;
                editor.putInt("SENSITIVITY", x);
                editor.commit();
                if (isRecording) {
                    startRecording();
                }

            }
        });
        thresholdSb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                x = i;
                thresholdTv.setText("Threshold = " + i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (haveThread) {
                    thread.interrupt();
                }

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                threshold = x;
                editor.putInt("THRESHOLD", x);
                editor.commit();

                if (isRecording) {
                    startRecording();
                }
            }
        });


    }

    public void startRecording() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) ==
                    PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) ==
                    PackageManager.PERMISSION_GRANTED) {
                //Version>=Marshmallow
                startAudioDispatcher();

            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO) || shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                    Toast.makeText(this,
                            "App required access to audio", Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CALL_PHONE
                }, REQUEST_AUDIO_PERMISSION_RESULT);
            }

        } else {
            //Version < Marshmallow
            startAudioDispatcher();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_AUDIO_PERMISSION_RESULT) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "Application will not have audio on record", Toast.LENGTH_SHORT).show();
            } else {
                startAudioDispatcher();
            }
        }
    }


    public void startAudioDispatcher() {

        dispatcher = AudioDispatcherFactory.fromDefaultMicrophone(44100, 2048, 0);



        PercussionOnsetDetector mPercussionDetector;
        mPercussionDetector = new PercussionOnsetDetector(44100, 2048,
                new OnsetHandler() {
                    @Override
                    public void handleOnset(double time, double salience) {
                        System.out.println("Time: " + time + " Claps:" + claps);
                        playAudio();
                    }
                }, sensitivity, threshold);
        dispatcher.addAudioProcessor(mPercussionDetector);
        thread = new Thread(dispatcher, "Audio Dispatcher");
        thread.start();
        haveThread = true;
    }


    public void playAudio() {

        if (!hasDetected && claps < 1) {
            claps++;
        } else {

            if (!hasDetected) {
                dispatcher.stop();
                hasDetected = true;
                thread.interrupt();
                startIntent();
                finish();
            }

        }
    }


    public void startIntent() {
        isRecording = false;
        haveThread = false;
        thread.interrupt();

        if (!delay_et.getText().toString().isEmpty()) {
            editor.putString("delay", delay_et.getText().toString().trim());
            editor.apply();
        }
        editor.putString("delay", delay_et.getText().toString().trim());
        editor.apply();
        String delay = delay_et.getText().toString().trim();
        String phoneNumber = phoneNumber_et.getText().toString().trim();
        Intent intent = new Intent(this, StopActivity.class);
        intent.putExtra("phoneNumber", phoneNumber);
//        if (!delay.isEmpty()) {
            intent.putExtra("delay", delay);
//        }
//        Intent i = new Intent(this, StopActivity.class);
        startActivity(intent);
//        startActivity(i);
        this.finish();


    }

}




