package com.example.detector;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class StopActivity extends AppCompatActivity {
    private static Handler mainThreadHandler;
    MainActivity mainActivity;
    MediaPlayer mp;
    AudioManager audioManager;
    int files = R.raw.alarmsound;
    boolean isFinished = false, hasDelay = false;
    int delay;
    String phoneNumberString, delayString;
    Timer t;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop);
        mainActivity = new MainActivity();
        phoneNumberString = getIntent().getStringExtra("phoneNumber");

        if (!getIntent().getStringExtra("delay").isEmpty()) {
            delayString = getIntent().getStringExtra("delay").trim();
            hasDelay = true;
            delay = Integer.parseInt(delayString);


        }
        isFinished = false;
        intent = new Intent(this, MainActivity.class);
        mp = MediaPlayer.create(this, files);
        audioManager = (AudioManager)
                this.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        audioManager.setSpeakerphoneOn(true);
        mp.setAudioStreamType(AudioManager.MODE_IN_COMMUNICATION);
        mp.setLooping(true);
        mp.start();

        //todo

        t = new Timer();
        if (hasDelay) {
            t.scheduleAtFixedRate(new TimerTask() {

                                      @Override
                                      public void run() {
                                          if (!isFinished && !phoneNumberString.isEmpty()) {
                                              finish();
                                              startActivity(intent);
                                              Uri number = Uri.parse("tel:" + phoneNumberString);
                                              Intent callIntent = new Intent(Intent.ACTION_CALL, number);
                                              isFinished = true;
                                              mp.stop();
                                              mp.release();
                                              startActivity(callIntent);
                                              t.cancel();
                                          }
                                      }

                                  },
                    delay * 1000, 99999999);
        }


        //todo
    }

    public void returnToMain(View view) {
        isFinished = true;
        startActivity(intent);
        mp.stop();
        mp.release();
        finish();


    }
}